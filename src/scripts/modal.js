
const handleDeleteModal = (evt) => {
    console.log("de");
  Swal.fire({
    title: "Are you sure ?",
    text: "This action cannot be undone. Remember, our connections are valuable.!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3E8EDE",
    cancelButtonColor: "#FE6F5E",
    confirmButtonText: "Yes, delete it!",
  }).then((result) => {
    if (result.isConfirmed) {
      Swal.fire({
        title: "Deleted!",
        text: "Contact removed successfully.",
        icon: "success",
        confirmButtonColor: "#3E8EDE",
      });
    }
  });
};
