const form = document.getElementById("contact-form");

const firstNameError = document.getElementById("firstNameError");
const lastNameError = document.getElementById("lastNameError");
const emailError = document.getElementById("emailError");
const phoneError = document.getElementById("phoneError");
const dobError = document.getElementById("dobError");
const addressError = document.getElementById("addressError");
const profileError = document.getElementById("profileError");

const err = document.querySelectorAll(".err");

validate.extend(validate.validators.datetime, {
  parse: function (value) {
    return +moment.utc(value);
  },
  format: function (value) {
    var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
    return moment.utc(value).format(format);
  },
});

const presenceValidation = {
  allowEmpty: false,
  message: "is required",
};

var constraints = {
  firstName: {
    presence: presenceValidation,
    length: {
      minimum: 2,
      maximum: 50,
      tooShort: `must be at least %{count} characters`,
      tooLong: `must be at most %{count} characters`,
    },
  },
  lastName: {
    presence: presenceValidation,
    length: {
      minimum: 2,
      maximum: 50,
      tooShort: `must be at least %{count} characters`,
      tooLong: `must be at most %{count} characters`,
    },
  },
  email: {
    presence: presenceValidation,
    email: {
      message: "is not valid",
    },
  },
  phoneNumber: {
    presence: presenceValidation,
    format: {
      pattern: "^[6789][0-9]{9}",
      message: "is not valid",
    },
  },
  dob: {
    presence: presenceValidation,
    datetime: {
      dateOnly: true,
    },
  },
  address: {
    presence: presenceValidation,
    length: {
      minimum: 5,
      tooShort: `must be at least %{count} characters`,
    },
  },
  profilePicture: {
    presence: presenceValidation,
  },
};

const validateForm = (evt) => {
  evt.preventDefault();
  const formValues = validate.collectFormValues(form);
  const validated = validate(formValues, constraints);
  if (validated !== undefined) {
    validated.firstName
      ? (firstNameError.innerText = validated.firstName[0])
      : (firstNameError.innerText = "");
    validated.lastName
      ? (lastNameError.innerText = validated.lastName[0])
      : (lastNameError.innerText = "");
    validated.email
      ? (emailError.innerText = validated.email[0])
      : (emailError.innerText = "");
    validated.phoneNumber
      ? (phoneError.innerText = validated.phoneNumber[0])
      : (phoneError.innerText = "");
    validated.dob
      ? (dobError.innerText = validated.dob[0])
      : (dobError.innerText = "");
    validated.address
      ? (addressError.innerText = validated.address[0])
      : (addressError.innerText = "");
    validated.profilePicture
      ? (profileError.innerHTML = validated.profilePicture[0])
      : (profileError.innerText = "");
  } else {
    form.reset();
    err.forEach((elem) => {
      elem.innerText = " ";
    });
  }
};


form.addEventListener("submit", validateForm);