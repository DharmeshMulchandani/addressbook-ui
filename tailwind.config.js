/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      colors: {
        'primary': '#3E8EDE',
        'primary-light': '#D8E8F8',
        'success': '#50C878',
        'success-light': '#CBEFD7',
        'danger': '#FE6F5E',
        'danger-light': '#FFD4CF',
        'lightGrey': '#F4F4F4',
      },
      boxShadow: {
        'custom-1': '0px 0px 15px 0px rgba(51, 51, 51, 0.15)',
      },
      gridTemplateColumns: {
        'column': '1fr 1fr 2fr 1fr 1fr 1fr 1fr'
      }
    },
  },
  plugins: [],
}

